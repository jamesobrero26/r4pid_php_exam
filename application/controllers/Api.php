<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
	public function __construct()
	{
		header("Content-Type: application/json");
		parent::__construct();
		$this->load->model('classicmodels');
		$this->load->helper('url');

		// get class puclic methods exept common for dymamic url
		$publicMethods = (new ReflectionClass($this))->getMethods(ReflectionMethod::IS_PUBLIC);
		$this->methods = array_filter($publicMethods, function($method) {
			return !in_array($method->name, [
				'__construct',
				'index',
				'get_instance'
			]);
		});

	}

	private function serialize($data) {
		$serializer = json_encode($data);

		if ($serializer === false) {
			http_response_code(500);
			return json_encode([
				'success'=> false,
				'message'=> "Internal server error!"
			]);
		}

		http_response_code(200);
		return json_encode($data);
	}

	public function index()
	{
		// create dymamic url documentation based on methods
		$urls = array_map(
			function ($method) {
				$url = base_url() . strtolower(get_class($this)) .'/'. $method->name . '/';
				$parameters = $method->getParameters();

				// get function parameters
				if (!empty($parameters)) {
					foreach ($parameters as $param) {
						$url .= "{{" . $param->name . '}}/';
					}
				}

				return $url;
			}, $this->methods);

		echo $this->serialize(
			['url' => array_values($urls)]
		);
	}

	/***
	 * Recursive function to get employee under
	 ***/
	private function generateOrganization(array $employees, $parentId = 0) {
		$branch = array();

		foreach ($employees as $employee) {
			if ($employee['reportsTo']  == $parentId) {
				unset($employee['reportsTo']);
				$employeeUnder = $this->generateOrganization($employees, $employee['employeeNumber']);

				if ($employeeUnder) {
					$employee['employeeUnder'] = $employeeUnder;
				}
				$branch[] = $employee;
			}
		}

		return $branch;
	}

	public function organization()
	{
		$employees = $this->classicmodels->employees();
		$organization = $this->generateOrganization($employees);
	  	echo $this->serialize($organization);
	}

	public function offices()
	{
		$offices = $this->classicmodels->offices();
		$officeEmployees = array_map(function($office) {
			$office['employees'] = $this->classicmodels->employee_office($office['officeCode']);
			return $office;
		}, $offices);
		echo $this->serialize($officeEmployees);
	}

	private function format_amount($amount) {
		return number_format($amount, 2, '.', '');
	}

	private function compute_commision(array $products) {
		$total = 0;
		foreach ($products as $key => $product) {
			list($antecedent, $consequent) = explode(':', $product['productScale']);
			$comission = ($antecedent / $consequent);
			$total += ($comission * $product['quantity'] * $product['sales']);
		}

		return $this->format_amount($total);
	}

	private function cleanProductData(&$products) {
		$products = array_map(function($product) {
			unset($product['productLine']);
			unset($product['productScale']);
			return $product;
		}, $products);
	}

	private function removeEmployeeWithNoSales(&$sales) {
		foreach ($sales as $key => $sale) {
			if (!isset($sale['productLines'])) {
				unset($sales[$key]);
			}
		}
		$sales = array_values($sales);
	}

	public function sales_report($employeeNumber=0)
	{
		// get representatives
		$repesentatives = $this->classicmodels->repesentatives($employeeNumber);

		$sales = array_map(function($repesentative) {
			$products = $this->classicmodels->representative_product($repesentative['employeeNumber']);

			// get distinct product line
			$productLineList = array_unique(array_column($products, 'productLine'), SORT_LOCALE_STRING);

			if (empty($productLineList)) {
				return $repesentative;
			}

			$repProductLines = array();

			// get products under productline
			foreach ($productLineList as $key => $list) {
				$productLine = $this->classicmodels->product_line($list)[0];
				if (empty($productLine)) {
					continue;
				}

				// filter products under product line
				$productsUnder = array_filter($products, function($product) use ($list) {
					return $product['productLine'] == $list;
				});

				$productLine['commision']  = $this->compute_commision($productsUnder);
				$productLine['quantity'] = $this->format_amount(array_sum(array_column($productsUnder, 'quantity')));
				$productLine['sales'] = $this->format_amount(array_sum(array_column($productsUnder, 'sales')));
				$this->cleanProductData($productsUnder);
				$productLine['products'] = array_values($productsUnder);

				$repProductLines[] = $productLine;
			}

			$repesentative['totalCommision'] = $this->format_amount(array_sum(array_column($repProductLines, 'commision')));
			$repesentative['totalSales'] = $this->format_amount(array_sum(array_column($repProductLines, 'sales')));
			$repesentative['productLines'] = $repProductLines;

			return $repesentative;
		}, $repesentatives);

		$this->removeEmployeeWithNoSales($sales);
		// if single emplyee, return object not list of object
		echo $this->serialize((!empty($employeeNumber) ? $sales[0] : $sales));
	}
}
