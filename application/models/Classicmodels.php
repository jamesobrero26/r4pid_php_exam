<?php
class Classicmodels extends CI_Model
{
	public function employees()
	{
		$this->db->select("employeeNumber, CONCAT(firstName, ' ', lastName) as name, jobTitle, reportsTo");
		$this->db->from('employees');
	  	$resultSet = $this->db->get();
	  	return $resultSet->result_array();
	}

	public function offices()
	{
		$this->db->select("officeCode, city");
		$this->db->from('offices');
	  	$resultSet = $this->db->get();
	  	return $resultSet->result_array();
	}

	public function employee_office($officeCode)
	{
		$this->db->select("employeeNumber, CONCAT(firstName, ' ', lastName) as name, jobTitle");
		$this->db->from('employees');
		$this->db->where('officeCode', $officeCode);
	  	$resultSet = $this->db->get();
	  	return $resultSet->result_array();
	}

	public function repesentatives($employeeNumber=0)
	{
		$this->db->select("
			emp.employeeNumber,
			CONCAT(emp.firstName, ' ', emp.lastName) as name,
			emp.jobTitle,
			emp.officeCode,
			ofc.city
		");
		$this->db->from('employees as emp');
		$this->db->join('offices as ofc', 'ofc.officeCode = emp.officeCode', 'left');

		if (!empty($employeeNumber)) {
			$this->db->where('employeeNumber', $employeeNumber);
		}

	  	$resultSet = $this->db->get();
	  	return $resultSet->result_array();
	}

	public function representative_product($salesRepEmployeeNumber=0) 
	{
		$query = $this->db->query("
			SELECT
		 	products.productCode,
			products.productName,
			products.productName,
			products.productLine,
			products.productScale,
			SUM(orderdetails.quantityOrdered) as quantity,
			SUM(orderdetails.quantityOrdered * orderdetails.priceEach) as sales,
			COUNT(orders.customerNumber) as numberOfCustomerBought
			FROM products
			JOIN orderdetails
				ON products.productCode = orderdetails.productCode
			JOIN orders
				ON orders.orderNumber = orderdetails.orderNumber and orders.status = 'Shipped'
			JOIN customers
				ON customers.customerNumber = orders.customerNumber
			WHERE customers.salesRepEmployeeNumber = " . $salesRepEmployeeNumber . "
			GROUP BY products.productCode
		");
	  	return $query->result_array();
	}

	public function product_line($productLine)
	{
		$this->db->select('productLine, textDescription');
		$this->db->from('productlines');
		$this->db->where('productLine', $productLine);
		$resultSet = $this->db->get();
		return $resultSet->result_array();
	}
}
